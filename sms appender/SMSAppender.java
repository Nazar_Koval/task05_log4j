package com.koval;

import java.io.Serializable;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.PatternLayout;

/**
 * Realization of SMS Appender
 */
@Plugin(name = "SMS", category = "Core", elementType = "appender", printObject = true)
public class SMSAppender extends AbstractAppender {

    /**
     * Constructor
     * @param str - message
     * @param filter - default filter
     * @param layout - default layout
     * @param ignoreExc - variable to ignore exceptions
     */
    protected SMSAppender(String str, Filter filter,
                          Layout<?extends Serializable>layout,final boolean ignoreExc){
        super(str,filter,layout,ignoreExc);
    }

    /**
     * Append SMS
     * @param logEvent default logger event
     */
    @Override
    public void append(LogEvent logEvent) {
        try {
            SMSExample.send(new String(getLayout().toByteArray(logEvent)));
        }catch (Exception ex){}
    }

    /**
     * Particular sms
     * @param name - name
     * @param layout - default layout
     * @param filter - default filter
     * @param otherAttribute - other attributes
     * @return new SMS appender
     */
    @PluginFactory
    public static SMSAppender createAppender(
            @PluginAttribute("name")String name,
            @PluginElement("Layout")Layout<?extends Serializable>layout,
            @PluginElement("Filter")final Filter filter,
            @PluginAttribute("otherAttribute")String otherAttribute){
        if(name == null){
            LOGGER.error("No name provided");
            return null;
        }
        if(layout == null){
            layout = PatternLayout.createDefaultLayout();
        }
        return new SMSAppender(name,filter,layout,true);
    }
}

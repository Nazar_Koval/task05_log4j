package com.koval;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

/**
 * Class to interact with Twilio.com
 */
public class SMSExample {

    /**
     * Account SID given from Twilio
     */
    public static String ACCOUNT_SID = "AC1bc5687739b1327fa7cdb968d8e7ef8b";

    /**
     * Authentication token given from Twilio
     */
    public static String AUTH_TOKEN = "bf13f5aad1e153eb59cba45c5677e46b";

    /**
     * Method to send messages
     * @param str - message
     */
    public static void send(String str){
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380679359820"),
                new PhoneNumber("+12056192494"),str).create();
    }
}

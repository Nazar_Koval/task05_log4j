package com.koval;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static com.koval.Task.*;

/**
 * Main class to experiment with loggers
 */
public class Main {
    /**
     * Logger in Main class
     */
    private static Logger logger_M = LogManager.getLogger(Main.class);

    /**
     * Default constructor
     */
    Main(){}
    /**
     *
     * @param args - command line arguments
     */
    public static void main(String[] args) {
        logger_M.fatal("Fatal test");
        logger_M.trace("Trace test");
        logger_M.error("Error test");
        logger_M.warn("Warn test");
        logger_M.debug("Debug test");

        logger_T.warn("Error test");
        logger_T.debug("debug");
        logger_T.info("info");
        logger_T.fatal("FATAL");
        logger_T.trace("trace");
    }
}

package com.koval;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Helping class to experiment with other logger
 */
public class Task {
    /**
     * Logger in Task class
     */
    public static Logger logger_T = LogManager.getLogger(Task.class);
}
